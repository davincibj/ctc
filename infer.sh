python infer.py --infer_dir=./data/infer/0/ \
	--checkpoint_dir=./checkpoint/ \
	--num_gpus=1 \
	--mode=infer \
	--image_height=60 \
	--image_width=352 \
	--batch_size=1 \
