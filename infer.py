import datetime
import logging
import os
import time

import cv2
import numpy as np
import tensorflow as tf

import cnn_lstm_otc_ocr
import utils
import helper

FLAGS = utils.FLAGS

logger = logging.getLogger('Traing for OCR using CNN+LSTM+CTC')
logger.setLevel(logging.INFO)
charset = '0123456789'
#===============================================================
def analyze(gt_txt = './infer.txt',result_txt = './result.txt',analyze_txt = './analyze.txt'):
    # compare ground true and result of infer imgs,output to analyze.txt
    gt_list = []
    res_list = []
    ana_list = []
    with open(gt_txt) as f:
        for line in f.readlines():
            line = line.strip()
            imgf_gt = os.path.abspath(line.split(':')[0])
            code = line.split(':')[1].split(',')
            gt_str = ''.join([charset[int(c)-1] for c in code])
            gt_list.append([imgf_gt,gt_str])
    gt_list.sort()
    with open(result_txt) as f:
        for line in f.readlines():
            line = line.strip()
            resf = os.path.abspath(line.split(':')[0])
            res_str = line.split(':')[1]                           
            res_list.append([resf,res_str])
    print gt_list[:5]
    print res_list[:5]
    
    assert len(gt_list)==len(res_list)
    for i in range(0,len(gt_list)):
        imgf_gt,gt_str = gt_list[i]
        imgf_res,res_str = res_list[i]
        
        assert imgf_gt == imgf_res
        item = imgf_gt,gt_str,res_str,gt_str==res_str
        #print item
        ana_list.append(item)
    tmp = np.array(ana_list)[:,-1]
    accuracy = np.sum(tmp=='True')/float(len(gt_list))
    print '=====>accuracy is: %f in %d/%d.' %(accuracy,np.sum(tmp=='True'),len(res_list))    
    
    with open(analyze_txt,'w') as f:
        f.write('=====>accuracy is: %f in %d/%d.' %(accuracy,np.sum(tmp=='True'),len(res_list)))
        for imgf_gt,gt_str,res_str,compare in ana_list:
            line = '%s\t%s\t%s\t%s\n' %(imgf_gt,gt_str,res_str,compare)
            f.write(line)
    
    
    return
#===============================================================
def train(train_lst=None, val_lst=None, mode='train'):
    model = cnn_lstm_otc_ocr.LSTMOCR(mode)
    model.build_graph()

    print('loading train data, please wait---------------------')
    train_feeder = utils.DataIterator(data_lst=train_lst)
    print('get image: ', train_feeder.size)

    
    print('loading validation data, please wait---------------------')
    val_feeder = utils.DataIterator(data_lst=val_lst)
    print('get image: ', val_feeder.size)

    num_train_samples = train_feeder.size  # 100000
    num_batches_per_epoch = int(num_train_samples / FLAGS.batch_size)  # example: 100000/100

    num_val_samples = val_feeder.size
    num_batches_per_epoch_val = int(num_val_samples / FLAGS.batch_size)  # example: 10000/100
    shuffle_idx_val = np.random.permutation(num_val_samples)
    print(num_batches_per_epoch, num_batches_per_epoch_val)

    with tf.device('/cpu:0'):
        config = tf.ConfigProto(allow_soft_placement=True)
        with tf.Session(config=config) as sess:
            sess.run(tf.global_variables_initializer())

            saver = tf.train.Saver(tf.global_variables(), max_to_keep=100)
            train_writer = tf.summary.FileWriter(FLAGS.log_dir + '/train', sess.graph)
            if FLAGS.restore:
                ckpt = tf.train.latest_checkpoint(FLAGS.checkpoint_dir)
                if ckpt:
                    # the global_step will restore sa well
                    saver.restore(sess, ckpt)
                    print('restore from the checkpoint{0}'.format(ckpt))

            print('=============================begin training=============================')
            for cur_epoch in range(FLAGS.num_epochs):
                shuffle_idx = np.random.permutation(num_train_samples)
                train_cost = 0
                start_time = time.time()
                batch_time = time.time()

                # the tracing part
                for cur_batch in range(num_batches_per_epoch):
                    if (cur_batch + 1) % 100 == 0:
                        print('batch', cur_batch, ': time', time.time() - batch_time)
                    batch_time = time.time()
                    indexs = [shuffle_idx[i % num_train_samples] for i in
                              range(cur_batch * FLAGS.batch_size, (cur_batch + 1) * FLAGS.batch_size)]
                    batch_inputs, batch_seq_len, batch_labels = \
                        train_feeder.input_index_generate_batch(indexs)
                    # batch_inputs,batch_seq_len,batch_labels=utils.gen_batch(FLAGS.batch_size)
                    feed = {model.inputs: batch_inputs,
                            model.labels: batch_labels,
                            model.seq_len: batch_seq_len}

                    # if summary is needed
                    # batch_cost,step,train_summary,_ = sess.run([cost,global_step,merged_summay,optimizer],feed)

                    summary_str, batch_cost, step, _ = \
                        sess.run([model.merged_summay, model.cost, model.global_step,
                                  model.train_op], feed)
                    # calculate the cost
                    train_cost += batch_cost * FLAGS.batch_size

                    train_writer.add_summary(summary_str, step)

                    # save the checkpoint
                    if step % FLAGS.save_steps == 1:
                        if not os.path.isdir(FLAGS.checkpoint_dir):
                            os.mkdir(FLAGS.checkpoint_dir)
                        logger.info('save the checkpoint of{0}', format(step))
                        saver.save(sess, os.path.join(FLAGS.checkpoint_dir, 'ocr-model'),
                                   global_step=step)

                    # train_err += the_err * FLAGS.batch_size
                    # do validation
                    if step % FLAGS.validation_steps == 0:
                        acc_batch_total = 0
                        lastbatch_err = 0
                        lr = 0
                        for j in range(num_batches_per_epoch_val):
                            indexs_val = [shuffle_idx_val[i % num_val_samples] for i in
                                          range(j * FLAGS.batch_size, (j + 1) * FLAGS.batch_size)]
                            val_inputs, val_seq_len, val_labels = \
                                val_feeder.input_index_generate_batch(indexs_val)
                            val_feed = {model.inputs: val_inputs,
                                        model.labels: val_labels,
                                        model.seq_len: val_seq_len}

                            dense_decoded, lr = \
                                sess.run([model.dense_decoded, model.lrn_rate],
                                         val_feed)

                            # print the decode result
                            ori_labels = val_feeder.the_label(indexs_val)
                            acc = utils.accuracy_calculation(ori_labels, dense_decoded,
                                                             ignore_value=-1, isPrint=True)
                            acc_batch_total += acc

                        accuracy = (acc_batch_total * FLAGS.batch_size) / num_val_samples

                        avg_train_cost = train_cost / ((cur_batch + 1) * FLAGS.batch_size)

                        # train_err /= num_train_samples
                        now = datetime.datetime.now()
                        log = "{}/{} {}:{}:{} Epoch {}/{}, " \
                              "accuracy = {:.3f},avg_train_cost = {:.3f}, " \
                              "lastbatch_err = {:.3f}, time = {:.3f},lr={:.8f}"
                        print(log.format(now.month, now.day, now.hour, now.minute, now.second,
                                         cur_epoch + 1, FLAGS.num_epochs, accuracy, avg_train_cost,
                                         lastbatch_err, time.time() - start_time, lr))
#============================================================================
def infer(img_path, mode='infer'):
    
    imgList = helper.load_img_path(img_path)
    imgList = helper.load_img_path(img_path)
    #print(imgList[:5])
    #print(len(imgList),imgList)

    model = cnn_lstm_otc_ocr.LSTMOCR(mode)
    model.build_graph()

    total_steps = len(imgList) / FLAGS.batch_size
    
    config = tf.ConfigProto(allow_soft_placement=True)
    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())

        saver = tf.train.Saver(tf.global_variables(), max_to_keep=100)
        ckpt = tf.train.latest_checkpoint(FLAGS.checkpoint_dir)
        if ckpt:
            saver.restore(sess, ckpt)
            print('restore from ckpt{}'.format(ckpt))
        else:
            print('cannot restore')

        decoded_expression = []
        imgf_list = []
        for curr_step in xrange(total_steps):

            imgs_input = []
            seq_len_input = []
            
            for img in imgList[curr_step * FLAGS.batch_size: (curr_step + 1) * FLAGS.batch_size]:
                #im = cv2.imread(img, 0).astype(np.float32) / 255.  
                #origin,  if shape of input img is not FLAGS.image_height/width,reshape the img will cause deform
                
                im = cv2.imread(img, 0) 
                im = utils.padding_resize(im) #weibin: add padding function to fit the FLAGS shape avoiding deform
                im = im.astype(np.float32) / 255.
                im = np.reshape(im, [FLAGS.image_height, FLAGS.image_width, FLAGS.image_channel])

                def get_input_lens(seqs):
                    length = np.array([FLAGS.max_stepsize for _ in seqs], dtype=np.int64)

                    return seqs, length

                inp, seq_len = get_input_lens(np.array([im]))
                imgs_input.append(im)
                seq_len_input.append(seq_len)
                imgf_list.append(img)

            imgs_input = np.asarray(imgs_input)
            seq_len_input = np.asarray(seq_len_input)
            seq_len_input = np.reshape(seq_len_input, [-1])

            feed = {model.inputs: imgs_input,
                    model.seq_len: seq_len_input}
            dense_decoded_code = sess.run(model.dense_decoded, feed)

            for item in dense_decoded_code:
                expression = ''

                for i in item:
                    if i == -1:
                        expression += ''
                    else:
                        expression += utils.decode_maps[i]
                        
                decoded_expression.append(expression)
            #print 'file name:',img
        zipped_results = zip(imgf_list,decoded_expression)
        print 'Total inferred:',len(imgf_list)#,len(decoded_expression)
        with open('./result.txt', 'w') as f:
            #for code in decoded_expression:
            #    f.write(code + '\n')
            for line in zipped_results:
                #print line[0]+':'+line[1]
                f.write(line[0]+':'+line[1]+'\n')
#============================================================================
def main(_):
    if FLAGS.num_gpus == 0:
        dev = '/cpu:0'
    elif FLAGS.num_gpus == 1:
        dev = '/gpu:0'
    else:
        raise ValueError('Only support 0 or 1 gpu.')

    with tf.device(dev):
        if FLAGS.mode == 'train':
            train(FLAGS.train_lst, FLAGS.val_lst, FLAGS.mode)

        elif FLAGS.mode == 'infer':
            infer(FLAGS.infer_dir, FLAGS.mode)
            analyze()

#============================================================================
if __name__ == '__main__':
    tf.logging.set_verbosity(tf.logging.INFO)
    tf.app.run()
